# Mixed Style Transfer

This project is a simple extension of the
[neural style transfer algorithm](https://arxiv.org/abs/1508.06576) developed
by Gatys et al. The code is largely based on the associated [PyTorch tutorial](https://pytorch.org/tutorials/advanced/neural_style_tutorial.html). By implementing a straightforward modification of the style loss
function it is possible to include and mix several different styles with arbitrary
weights. For two style images _**A**_ and _**B**_, the modified style loss reads

![style loss definition](/readme_files/equation_style_loss.png).

Here, _**r**_ ∈ [0, 1] denotes the relative weight ratio between _**A**_ and _**B**_. This allows for a smooth interpolation between the two styles. The individual loss values are calculated in terms of Gram matrices as given in the original paper.

To demonstrate the idea, we use the content and style images from the PyTorch tutorial as well as an additional painting by Pablo Picasso:

![content image](/readme_files/content.jpg) ![style image A](/readme_files/style1.jpg) ![style image B](/readme_files/style2.jpg)

We run the algorithm using _**r**_ ∈ {0, 0.25, 0.5, 0.75, 1} for 500 iterations each, producing some interesting mixed styles:

![interpolation image](/readme_files/interpolation.png)

This method can in principle be developed much further, beyond simple mash-ups of two different styles. For example, by varying the layers and associated weights that are used in the calculation of the style losses, it may be possible to use the color scheme of one image and the drawing style of the other. Mixing several images from the same artist may produce some unique 'paintings'. There are no limits to your creativity!

## Usage

Executing the script requires python3 and PyTorch, as well as some auxiliary packages for plotting and such. Run it by:

```
python transfer.py --content ./path/to/content/image --style1 ./path/to/style/image1 --style2 ./path/to/style/image2 \
--ratio [ratio] --weight [style weight] --steps [number of iteration steps]
```

The ratio, style weight and number of steps have reasonable built-in default values and are hence optional. A working example using the files in this project is e.g.:

```
python transfer.py --content ./images/dancing.jpg --style1 ./images/picasso_a.jpg --style2 ./images/picasso_b.jpg
```

This produces the image at the center displayed above.